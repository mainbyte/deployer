package security

import (
	"log"
	"io"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/rand"
)

var rsaPrivateKey *rsa.PrivateKey
var rsaKeySize int = 4096

func init() {
	var err error
	var random io.Reader = rand.Reader

	rsaPrivateKey, err = rsa.GenerateKey(random, rsaKeySize)

	if err != nil {
		panic(err)
	}
}

func EncryptRSA(message []byte) ([]byte, error) {
	var err error
	var messageBytes []byte
	var label []byte // can be empty
	var ciphertext []byte

	messageBytes = []byte(message)

	ciphertext, err = rsa.EncryptOAEP(sha512.New(), rand.Reader, &rsaPrivateKey.PublicKey, messageBytes, label)
	if err != nil {
		log.Println("Error from encryption: %s\n", err)
		return []byte{}, err
	}

	return ciphertext, nil
}

func DecryptRSA(ciphertext []byte) ([]byte, error) {
	var err error
	var label []byte // can be empty
	var plaintext []byte

	plaintext, err = rsa.DecryptOAEP(sha512.New(), rand.Reader, rsaPrivateKey, ciphertext, label)

	if err != nil {
		log.Println("Error from decryption: %s\n", err)
		return []byte{}, err
	}

	return plaintext, nil
}