package utils

import (
	"os"
	"fmt"
	"log"
	"encoding/json"
)

var env string
var envFilePath string
var possibleEnvs = []string{ "dev", "prod" }

type TransmitterConfig struct {
	ClientKey string `json:"clientKey"`
	ServerAddress string `json:"serverAddress"`
	Port int `json:"port"`
	DirPath string `json:"dirPath"`
}

type ReceiverConfig struct {
	ClientKey string `json:"clientKey"`
	ServerURL string `json:"serverURL"`
}

type ServerConfig struct {
	Port int `json:"port"`
	MongoURL string `json:"mongoURL"`
}

func LoadServerConfig() ServerConfig {
	var err error
	var configBytes []byte
	var config ServerConfig
	var envIsValid = false

	env = os.Getenv("ENV")

	for _, allowedEnv := range possibleEnvs {

		if env == allowedEnv {
			envIsValid = true
		}
	}

	if !envIsValid {
		log.Panic("Invalid env!")
	}

	configBytes, err = os.ReadFile(fmt.Sprintf("env/%s.json", env))

	if err != nil {
		panic(err)
		return ServerConfig{}
	}

	err = json.Unmarshal(configBytes, &config)

	if err != nil {
		panic(err)
		return ServerConfig{}
	}

	return config
}

func LoadTransmitterConfig() TransmitterConfig {
	var err error
	var configBytes []byte
	var config TransmitterConfig
	var envIsValid = false

	env = os.Getenv("ENV")

	for _, allowedEnv := range possibleEnvs {

		if env == allowedEnv {
			envIsValid = true
		}
	}

	if !envIsValid {
		log.Panic("Invalid env!")
	}

	configBytes, err = os.ReadFile(fmt.Sprintf("env/%s.json", env))

	if err != nil {
		panic(err)
		return TransmitterConfig{}
	}

	err = json.Unmarshal(configBytes, &config)

	if err != nil {
		panic(err)
		return TransmitterConfig{}
	}

	return config
}

func loadReceiverConfig() {

}