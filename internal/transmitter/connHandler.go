package transmitter

import (
	"fmt"
	"log"
	"net"
	"bufio"
)

func AuthToServer(conn net.Conn) {
	var err error
	var response string

	fmt.Fprintf(conn, "auth data\n")
	response, err = bufio.NewReader(conn).ReadString('\n')

	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println(response)
}