package server

import (
	"fmt"
	"net"
	"log"
	"bufio"

	// "deployer/pkg/security"
)

func HandleClient(conn net.Conn) {
	var err error
	// var cyphertext []byte
	// var plaintext []byte
	var response string

	log.Println(conn.RemoteAddr)

	response, err = bufio.NewReader(conn).ReadString('\n')

	if err != nil {
		log.Println(err.Error())
		return
	}

	fmt.Println(response)
	fmt.Fprintf(conn, "responseReceived\n")

	// cyphertext, err = security.EncryptRSA([]byte("test"))

	// if err != nil {
	// 	log.Println("Failed to encrypt RSA!")
	// 	return
	// }

	// plaintext, err = security.DecryptRSA(cyphertext)

	// if err != nil {
	// 	log.Println("Failed to decrypt RSA!")
	// 	return
	// }

	// log.Println("plaintext: " + string(plaintext))
}