package main

import (
	"fmt"
	"log"
	"net"

	"deployer/pkg/utils"
	"deployer/internal/transmitter"
)

func main() {
	var err error
	var conn net.Conn
	var config utils.TransmitterConfig

	config = utils.LoadTransmitterConfig()

	log.Println(fmt.Sprintf("%s:%d", config.ServerAddress, config.Port))

	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", config.ServerAddress, config.Port))

	if err != nil {
		panic(err)
	}

	transmitter.AuthToServer(conn)
}