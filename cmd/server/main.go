package main

import (
	"fmt"
	"log"
	"net"

	"deployer/pkg/utils"
	"deployer/internal/server"
)

func main() {
	var err error
	var config utils.ServerConfig
	var conn net.Conn
	var listener net.Listener

	config = utils.LoadServerConfig()

	listener, err = net.Listen("tcp", fmt.Sprintf(":%d", config.Port))

	if err != nil {
		panic(err)
		return
	}

	log.Println(fmt.Sprintf("Server started on port %d!", config.Port))

	for {
		conn, err = listener.Accept()

		if err != nil {
			log.Println(err.Error())
		}

		go server.HandleClient(conn)
	}
}