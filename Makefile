services := $(shell ls cmd)

build: $(services)

$(services):
	go build -o bin/$@ cmd/$@/main.go